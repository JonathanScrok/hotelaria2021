package enumerations;

public enum ProdutoSituacao {
	
    DISPONIVEL(1, "Disponivel"),
    INDISPONIVEL(2, "Indisponivel"),
    UTILIZADO(3, "Utilizado");

    private int id;
    private String nome;

    private ProdutoSituacao(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }
    
    public int getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }
}
