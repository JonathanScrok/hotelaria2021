package enumerations;

public enum QuartoSituacao {

    DISPONIVEL(1, "Disponivel"),
    INDISPONIVEL(2, "Indisponivel"),
    FECHADO(3, "Fechado"),
    OCUPADO(4, "Ocupado");

    private int id;
    private String nome;

    QuartoSituacao(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }
    
    public int getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }
}
