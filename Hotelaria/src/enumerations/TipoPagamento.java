package enumerations;

public enum TipoPagamento {

    CREDITO(1, "Credito"),
    DEBITO(2, "Debito"),
    TRANSFERENCIAELETRONICA(3, "TransferenciaEletronica"),
    NFC(3, "Nfc"),
    DINHEIRO(3, "Dinheiro");

    private int id;
    private String nome;

    private TipoPagamento(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }
    
    public int getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }
}
