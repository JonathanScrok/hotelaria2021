package enumerations;

public enum TipoQuarto {
	
    CASAL(1, "Casal"),
    SOLTEIRO(2, "Solteiro"),
    DUALCASAL(3, "DualCasal");

    private int id;
    private String nome;

    private TipoQuarto(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }
    
    public int getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

}
