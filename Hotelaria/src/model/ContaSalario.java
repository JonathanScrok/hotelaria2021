package model;

public class ContaSalario {
	private int id;
	private int conta;
	private String banco;
	private int agencia;
	
	private CarteiraTrabalho carteiraTrabalho;
	
	public void setCarteiraTrabalho(CarteiraTrabalho carteiraTrabalho) {
		this.carteiraTrabalho = carteiraTrabalho;
	}

	public ContaSalario(CarteiraTrabalho carteiraTrabalho) {
		this.carteiraTrabalho = carteiraTrabalho;
	}

	public CarteiraTrabalho getCarteiraTrabalho() {
		return carteiraTrabalho;
	}
}
