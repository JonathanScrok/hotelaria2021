package model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import enumerations.TipoQuarto;

public class AgendaDiario {

	private LocalDate dia = LocalDate.now();
	private int qtdQuartosDisponivelCasal;
	private int qtdQuartosDisponivelSolteiro;
	private int qtdQuartosDisponivelDualCasal;
	private Hotel hotel;
	
	public AgendaDiario(Hotel hotel) {
		this.hotel = hotel;
		this.hotel.adicionarDiaAgenda(this);
	}
	
	public int quartoDisponivelPorTipo(TipoQuarto type) {
		int quantidade = hotel.quartoDisponivelPorTipo(type, LocalDateTime.now(), LocalDateTime.now());
		return quantidade;
	}

	public int getQtdQuartosDisponivelDualCasal() {
		return qtdQuartosDisponivelDualCasal;
	}

	public void setQtdQuartosDisponivelDualCasal(int qtdQuartosDisponivelDualCasal) {
		this.qtdQuartosDisponivelDualCasal = qtdQuartosDisponivelDualCasal;
	}

	public int getQtdQuartosDisponivelSolteiro() {
		return qtdQuartosDisponivelSolteiro;
	}

	public void setQtdQuartosDisponivelSolteiro(int qtdQuartosDisponivelSolteiro) {
		this.qtdQuartosDisponivelSolteiro = qtdQuartosDisponivelSolteiro;
	}

	public int getQtdQuartosDisponivelCasal() {
		return qtdQuartosDisponivelCasal;
	}

	public void setQtdQuartosDisponivelCasal(int qtdQuartosDisponivelCasal) {
		this.qtdQuartosDisponivelCasal = qtdQuartosDisponivelCasal;
	}

	public LocalDate getDia() {
		return dia;
	}

	public void setDia(LocalDate dia) {
		this.dia = dia;
	}
	
}
