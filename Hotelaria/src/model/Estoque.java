package model;

import java.util.List;

public class Estoque {
	private int id;
	private String descricao;
	private double valor;
	private List<Produto> produtos;
	
	public Estoque(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public void adicionarProduto(Produto produto) {
		produtos.add(produto);
	}
	
	public void removerProduto(Produto produto) {
		produtos.remove(produto);
	}
	
	
}
