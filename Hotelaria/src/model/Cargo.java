package model;

public class Cargo {
	private int id;
	private String nome;
	private String descricao;

	public Cargo(){

	}
	
	public void adicionarCargo(Cargo cargo) {
		this.nome = cargo.nome;
		this.descricao = cargo.descricao;
	}
	
	public void removerCargo(Cargo cargo){
		this.nome = null;
		this.descricao = null;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
