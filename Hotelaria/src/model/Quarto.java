package model;

import enumerations.QuartoSituacao;
import enumerations.TipoQuarto;

public class Quarto {

	private int qtdCamas;
	private int qtdJanelas;
	private int numero;
	private TipoQuarto tipoquarto;
	private QuartoSituacao quartoSituacao = QuartoSituacao.DISPONIVEL;
	private Hotel hotel;
	private Estadia estadia;
	
	public Quarto(TipoQuarto tipoquarto, QuartoSituacao quartoSituacao, Hotel hotel, Estadia estadia) {
		if(estadia != null) {
			this.estadia = estadia;
		}
		this.hotel = hotel;
		this.tipoquarto = tipoquarto;
		this.quartoSituacao = quartoSituacao;
	}

	public void fazerReserva() {
	}
	
	private void definirStatusIndisponivel() {
		this.quartoSituacao = QuartoSituacao.INDISPONIVEL;
	}
	
	private void definirStatusFechado() {
		this.quartoSituacao = QuartoSituacao.FECHADO;
	}
	
	private void liberarQuarto(Quarto quarto) {
		this.quartoSituacao = QuartoSituacao.DISPONIVEL;
	}
	
	//

	public int getQtdCamas() {
		return qtdCamas;
	}

	public int getQtdJanelas() {
		return qtdJanelas;
	}

	public int getNumero() {
		return numero;
	}

	public TipoQuarto getTipoquarto() {
		return tipoquarto;
	}

	public QuartoSituacao getQuartoSituacao() {
		return quartoSituacao;
	}

	public void setQuartoSituacao(QuartoSituacao quartoSituacao) {
		this.quartoSituacao = quartoSituacao;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public Estadia getEstadia() {
		return estadia;
	}
	
	
}
