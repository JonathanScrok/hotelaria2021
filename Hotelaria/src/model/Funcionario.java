package model;

import java.time.LocalDate;
import java.util.List;

public class Funcionario {
	
	private int id;
	private String nome;
	private LocalDate dataNascimento;
	private Contrato contrato;
	private Endereco endereco;
	private List<Cargo> cargos;
	private List<Produto> produtos;
	private List<Servico> servicos;
	private List<Hotel> hoteis;
	
	public Funcionario(Contrato contrato, Endereco endereco, List<Cargo> cargos, List<Produto> produtos, List<Servico> servicos, List<Hotel> hoteis) {
		this.contrato = contrato;
		this.endereco = endereco;
		this.cargos = cargos;
		this.produtos = produtos;
		this.servicos = servicos;
		this.hoteis = hoteis;
	}
	
	public void realizarServico(TipoServico tiposervico, Quarto quarto) {
		
	}
	
}
