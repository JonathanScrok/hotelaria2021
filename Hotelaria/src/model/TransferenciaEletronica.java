package model;

public class TransferenciaEletronica {
	
	private String contaOrigem;
	private String contaDestino;
	private int cpfContaDestino;
	private String moeda;
	private Pagamento pagamento;
	
	public TransferenciaEletronica(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
	
	public String getContaOrigem() {
		return contaOrigem;
	}

	public String getContaDestino() {
		return contaDestino;
	}

	public int getCpfContaDestino() {
		return cpfContaDestino;
	}

	public String getMoeda() {
		return moeda;
	}
	
}
