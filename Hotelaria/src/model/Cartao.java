package model;

import java.time.LocalDate;

public class Cartao {
	
	private int CardNumber;
	private String bandeira;
	private String titular;
	private String moeda;
	private LocalDate validade;
	private Pagamento pagamento;
	
	public Cartao(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
	
	public int getCardNumber() {
		return CardNumber;
	}
	
	public void setCardNumber(int cardNumber) {
		CardNumber = cardNumber;
	}
	public String getBandeira() {
		return bandeira;
	}

	public String getTitular() {
		return titular;
	}

	public String getMoeda() {
		return moeda;
	}

	public LocalDate getValidade() {
		return validade;
	}

	
}
