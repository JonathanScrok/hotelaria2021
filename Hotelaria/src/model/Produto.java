package model;

import java.time.LocalDate;

import enumerations.ProdutoSituacao;

public class Produto {
	private int id;
	private String nome;
	private String descricao;
	private String reuso;
	private LocalDate dataUso;
	private Estoque estoque;
	private ProdutoSituacao produtoSituacao;

	public Produto() {
	}
	
	public Produto(int id) {
	}

	public Produto(ProdutoSituacao produtoSituacao) {
		this.produtoSituacao = produtoSituacao;
	}
	
	public void utilizarProduto(int id) {
		Produto produto  = new Produto(id);
		estoque.removerProduto(produto);
	}
	
	public void devolverProduto(Produto produto) {
		estoque.adicionarProduto(produto);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getReuso() {
		return reuso;
	}

	public void setReuso(String reuso) {
		this.reuso = reuso;
	}

	public LocalDate getDataUso() {
		return dataUso;
	}

	public void setDataUso(LocalDate dataUso) {
		this.dataUso = dataUso;
	}

	public Estoque getEstoque() {
		return estoque;
	}

	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}

	public ProdutoSituacao getProdutoSituacao() {
		return produtoSituacao;
	}

	public void setProdutoSituacao(ProdutoSituacao produtoSituacao) {
		this.produtoSituacao = produtoSituacao;
	}
	
	
}
