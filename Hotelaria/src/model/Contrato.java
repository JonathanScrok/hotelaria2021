package model;

import java.time.LocalDateTime;

public class Contrato {
	
	private int id;
	private LocalDateTime dataAdimissao;
	private LocalDateTime dataDemissao;
	private String motivoDemissao;
	private ContaSalario contaSalario;
	
	private Funcionario funcionario;
	
	public Contrato(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
}
