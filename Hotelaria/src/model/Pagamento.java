package model;

import java.util.List;

import enumerations.TipoPagamento;

public class Pagamento {

    private double total;
    private List<TipoPagamento> tiposPagamentos;
    private Estadia estadia;

    public Pagamento() {
    }

    public Pagamento(List<TipoPagamento> tiposPagamentos, Estadia estadia) {
        this.tiposPagamentos = tiposPagamentos;
        this.estadia = estadia;
    }

    public void realizarPagamentoInicial() {
        double parseValue = this.total * 0.3;
        this.total = total - parseValue;
    }

    public boolean pagamentoRealizado(double value, Object pagamento) {
        return true;
    }

    public void realizarPagamentoFinal(double total) {
        this.total = total;
    }
}
