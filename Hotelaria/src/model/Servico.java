package model;

import java.time.LocalDateTime;
import java.util.List;

public class Servico {
	
	private int id;
	private LocalDateTime requisicaoServico;
	private LocalDateTime finalizacaoServico;
	private TipoServico tiposervico;
	private Hotel hotel;
	private List<Hospede> hospedes;
	
	public Servico(TipoServico tiposervico, Hotel hotel, List<Hospede> hospedes) {
		this.tiposervico = tiposervico;
		this.hotel = hotel;
		this.hospedes = hospedes;
	}
	
	public LocalDateTime getRequisicaoServico() {
		return requisicaoServico;
	}

	public void setRequisicaoServico(LocalDateTime requisicaoServico) {
		this.requisicaoServico = requisicaoServico;
	}

	public LocalDateTime getFinalizacaoServico() {
		return finalizacaoServico;
	}

	public void setFinalizacaoServico(LocalDateTime finalizacaoServico) {
		this.finalizacaoServico = finalizacaoServico;
	}

	public void adicionarFuncionarioServico(Funcionario funcionario) {
		setRequisicaoServico(LocalDateTime.now());
	}
	
	public void finalizarServico(int id) {		
	}

	public TipoServico getTiposervico() {
		return tiposervico;
	}

	public void setTiposervico(TipoServico tiposervico) {
		this.tiposervico = tiposervico;
	}
	
	
}
