package model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class Hospede {

	private int id;
	private String nome;
	private int celular;
	private boolean dependente;
	private Reserva reserva;
	private Endereco endereco;
	private List<Reserva> reservas;
	
	public Hospede(Endereco endereco, List<Reserva> reservas) {
		this.endereco = endereco;
		this.reservas = reservas;
	}
	
	 public void temReserva(LocalDate date) {
	 }
	 
	 public void cancelarReserva(LocalDateTime dateHospedagem) {
		 
		 LocalDateTime datehoje = LocalDateTime.now(); 
		 LocalDateTime datehosa7dias = dateHospedagem.minusDays(7);
		 LocalDateTime datehosa1dias = dateHospedagem.minusDays(1);
		 
		 if (datehoje.isBefore(datehosa7dias) || datehoje.equals(datehosa7dias)) {
			//reembolso do valor pago ser� integral
		}
		else if(datehoje.isAfter(datehosa7dias) && datehoje.isBefore(datehosa1dias)) {
			//multa de 30%
		}else if(datehoje.equals(dateHospedagem)) {
			//multa ser� de 70%
		}else {
			//n�o ser� realizado o reembolso
		}
	 }
}
