package model;

import java.time.LocalDateTime;
import java.util.List;

import enumerations.TipoQuarto;

public class Hotel {

    private int id;
    private String nome;
    private int qtdQuartos;
    private int qtdQuartosCasal;
    private int qtdQuartosSolteiro;
    private int qtdQuartoDualCasal;
    private List<Reserva> reservas;
    private List<Quarto> quartos;
    private List<AgendaDiario> agendasDiarias;
    private Endereco endereco;
    private List<Servico> servicos;

    public Hotel(List<Reserva> reservas, List<Quarto> quartos, List<AgendaDiario> agendasDiarias, Endereco endereco, List<Servico> servicos) {
        this.reservas = reservas;
        this.quartos = quartos;
        this.agendasDiarias = agendasDiarias;
        this.endereco = endereco;
        this.servicos = servicos;
    }

    public void adicionarDiaAgenda(AgendaDiario agenda) {
        this.agendasDiarias.add(agenda);
    }

    public boolean quartoDisponivel(LocalDateTime dataCheckIn, LocalDateTime dataCheckOut) {
        if (LocalDateTime.now().isAfter(dataCheckIn) && LocalDateTime.now().isBefore(dataCheckOut)) {
            return true;
        } else {
            return false;
        }
    }

    public int quartoDisponivelPorTipo(TipoQuarto tipo, LocalDateTime dataCheckIn, LocalDateTime dataCheckOut) {
        return 0;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }


    public int getQtdQuartos() {
        return qtdQuartos;
    }


    public List<Reserva> getReservas() {
        return reservas;
    }

    public List<Quarto> getQuartos() {
        return quartos;
    }


    public List<AgendaDiario> getAgendasDiarias() {
        return agendasDiarias;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servico> servicos) {
        this.servicos = servicos;
    }

	public int getQtdQuartosCasal() {
		return qtdQuartosCasal;
	}

	public int getQtdQuartosSolteiro() {
		return qtdQuartosSolteiro;
	}

	public int getQtdQuartoDualCasal() {
		return qtdQuartoDualCasal;
	}

    
}
