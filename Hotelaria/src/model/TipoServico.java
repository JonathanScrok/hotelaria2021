package model;

public class TipoServico {
    private int id;
    private String descricaoServico;

    public TipoServico(int id, String descricaoServico){

        this.id = id;
        this.descricaoServico = descricaoServico;
    }
}
