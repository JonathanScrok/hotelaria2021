package model;

import java.util.List;

public class Estadia {

	private int id;
	private int tipoQuarto;
	private Quarto quarto;
	private List<Pagamento> pagamentos;
	private List<Gasto> gastos;
	private Funcionario funcionario;
	
	public Estadia(Quarto quarto, List<Pagamento> pagamentos, List<Gasto> gastos, Funcionario funcionario) {
		this.quarto = quarto;
		this.funcionario = funcionario;
		this.pagamentos = pagamentos;
		if(gastos != null) {
			this.gastos = gastos;
		}
		
	}
	
	public void checkout() {
		TipoServico tipoServico = new TipoServico(id, "Servico de Camareira");
		funcionario.realizarServico(tipoServico, this.quarto);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTipoQuarto() {
		return tipoQuarto;
	}

	public void setTipoQuarto(int tipoQuarto) {
		this.tipoQuarto = tipoQuarto;
	}

	public Quarto getQuarto() {
		return quarto;
	}

	public void setQuarto(Quarto quarto) {
		this.quarto = quarto;
	}

	public List<Pagamento> getPagamentos() {
		return pagamentos;
	}

	public void setPagamentos(List<Pagamento> pagamentos) {
		this.pagamentos = pagamentos;
	}

	public List<Gasto> getGastos() {
		return gastos;
	}

	public void setGastos(List<Gasto> gastos) {
		this.gastos = gastos;
	}
	
}
