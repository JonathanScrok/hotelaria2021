package model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import enumerations.TipoQuarto;

public class Reserva {
	
	private int id;
	private Date checkIn;
	private Date checkOut;
	private String status;
	private boolean necessitaBerco;
	private Hotel hotel;
	private Hospede hospede;
	private TipoQuarto tipoquarto;
	
	public Reserva(Hotel hotel, Hospede hospede, TipoQuarto tipoquarto) {
		this.hotel = hotel;
		this.hospede = hospede;
		this.tipoquarto = tipoquarto;
	}
	
	public void checkStatus() {
	}

	public void cancelarReserva(){
		this.hospede.cancelarReserva(LocalDateTime.now());
	}
	
	public void checkIn() {
		Pagamento pagamento = new Pagamento();
		pagamento.realizarPagamentoInicial();
		enviarEmail();
	}
	
	public void enviarEmail() {
		//email enviado
	}
	
	public boolean verificarData(LocalDate date) {

		return false;
	}
}
